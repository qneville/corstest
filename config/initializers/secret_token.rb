# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
CorsTest::Application.config.secret_key_base = 'cfebc8c0b58cb1bd6cd6421b1d07c430691d19e5f038d9fd9cb3d013db1753798da241db14a4336bbf8c2f99951e17d2b3594a33197cd4d684f09aa4710f7b3c'
